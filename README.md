# Blog projemin HTML dökümanı

## Nedir?

Bu proje daha sonra django ve nuxt altyapısına geçirelecek tasarımın html dilinde oluşturulmuş önizlemesidir. 

## Tasarımda ne kullandım

Tasarımda herhangi bir css ve js kütüphanesi kullanmadım. CSS tasarımının temelinde **flex-box** yatıyor. Renkleri ve genel dizaynı dribble'da gördüğüm bir dashboard projesinden esinlendim. Dil olarak sass scss kullandım. Sass kullanmama rağmen kod tekrarı ve dağınıklık oldu o da deneyimsizlikten :)

JS kısmında şimdilik basit query seletor ve dom manipülasyonları kullandım. İleriki seviyelerde projeyi **nuxt js** kütüphanesine geçirerek frontendini güçlendirmeyi düşünüyorum. Backend server olarak ise **django** kullanmayı düşünüyorum. Rest api kısmında ise **django rest framework** işime yarar ancak önce iyice öğrenmem gerekiyor :)

## Yapılacaklar

- [ ] Mobil menüyü oluşturmak (üşendim... direkt nuxt js de yapmayı düşünüyorum)
- [ ] Arama kısmına animasyon eklemek
- [ ] Tasarıma kategoriler ve etiketler eklemek
- [ ] Pagination eklemek

> büyük ihtimalle yapılacaklar kısmını nuxt'a geçirirken hallederim.

## Projeden resimler

Masaüstü:

![Masaüstü görünümü](pics/masaustu.png)


Mobil:

![Masaüstü görünümü](pics/mobil.png)


Tablet:

![Masaüstü görünümü](pics/high_tablet.png)